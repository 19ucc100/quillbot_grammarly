import "./App.css";
import Navbar from "./components/Navbar";
import FeaturesSection from "./components/FeaturesSection";
import PremiumSection from "./components/PremiumSection";
import CssBaseline from "@mui/material/CssBaseline";

const App = () => {
  return (
    <div className="App">
      <CssBaseline />
      <Navbar />
      <FeaturesSection />
      <PremiumSection />
    </div>
  );
};

export default App;
