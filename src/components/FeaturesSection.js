import React, { useState, useEffect, useRef } from "react";
import {
  Box,
  Button,
  Container,
  Grid,
  LinearProgress,
  Typography
} from "@mui/material";
import { makeStyles } from "@material-ui/core";

// Lists of features data
const previewList = [
  {
    exampleImage:
      "https://static.grammarly.com/assets/files/1495248700340626177a3f4fb713a94d/writing_sample.svg",
    messageImage:
      "https://static.grammarly.com/assets/files/32847965b20166d16b376b3f97d7ad65/grammarly_suggestion.svg",
    exaImgMobile:
      "https://static.grammarly.com/assets/files/c1615dfecbbc277259f60c7461994468/mobile.png",
    msgImgMobile:
      "https://static.grammarly.com/assets/files/42e0587aa278ef8c3a3ecab5be8d42c3/mobile@2x.png 2x",
    alt: "Rephrase Sentence"
  },
  {
    exampleImage:
      "https://static.grammarly.com/assets/files/718a361635f4474a3b373645dce51bec/writing_sample.svg",
    messageImage:
      "https://static.grammarly.com/assets/files/299da6fd3e0803d745cb860de475a4e5/grammarly_suggestion.svg",
    exaImgMobile:
      "https://static.grammarly.com/assets/files/c1615dfecbbc277259f60c7461994468/mobile.png",
    msgImgMobile:
      "https://static.grammarly.com/assets/files/42e0587aa278ef8c3a3ecab5be8d42c3/mobile@2x.png 2x",
    alt: "Enhance word choice"
  },
  {
    exampleImage:
      "https://static.grammarly.com/assets/files/467997bcab46bc3b210b11951fa061d8/writing_sample.svg",
    messageImage:
      "https://static.grammarly.com/assets/files/8557e8eb1af323bdc9bb1cc8acff9bb7/grammarly_suggestion.svg",
    exaImgMobile:
      "https://static.grammarly.com/assets/files/1028e942a570cd89b44a5914cfc9eba7/mobile.png",
    msgImgMobile:
      "https://static.grammarly.com/assets/files/ccc513909b90c59f62277a3493cfc1dc/mobile@2x.png 2x",
    alt: "Add punctuation"
  },
  {
    exampleImage:
      "https://static.grammarly.com/assets/files/18c5385a8f59a60ee1a389426a2a0b89/writing_sample.svg",
    messageImage:
      "https://static.grammarly.com/assets/files/d9d689c835ee698bca792fe86759fbe1/grammarly_suggestion.svg",
    exaImgMobile:
      "https://static.grammarly.com/assets/files/c38481a57a0c10a84deafe5ca21d6eac/mobile.png",
    msgImgMobile:
      "https://static.grammarly.com/assets/files/90b995e0a51631a514394463af78cc81/mobile@2x.png 2x",
    alt: "Strike the right tone"
  }
];

const featuresList = [
  {
    title: "Clear, confident communication",
    subTitle: "Take the guesswork out of great writing."
  },
  {
    title: "Comprehensive real-time feedback",
    subTitle: "Effective writing takes more than good grammar."
  },
  {
    title: "Support you can rely on",
    subTitle: "Write with a second pair of eyes that never gets tired."
  },
  {
    title: "Strike the right tone",
    subTitle: "Make the best impression, every time."
  }
];

// Custom style classes
const styling = {
  section: {
    background:
      "url(https://static.grammarly.com/assets/files/06666c30f97cce25ac1cf1c4573332c2/swoosh.png) no-repeat left -150px top -70px",
    padding: "0 24px",
    minHeight: "680px"
  },
  h1: {
    marginBottom: "24px",
    paddingTop: "32px",
    fontFamily: '"DM Sans", sans-serif',
    fontSize: "42px",
    fontWeight: "700",
    lineHeight: "52px"
  },
  button: {
    width: "284px",
    height: "45px",
    textTransform: "none",
    backgroundColor: "#11a683",
    fontFamily: '"Inter", sans-serif',
    fontSize: "14px",
    lineHeight: "32px",
    fontWeight: "700"
  },
  indicatorItem: {
    fontFamily: "Inter,sans-serif",
    textAlign: "left",
    marginBottom: "20px",
    cursor: "pointer"
  },
  indicatorItemHeader: {
    fontFamily: "Inter,sans-serif",
    fontSize: "16px",
    lineHeight: "24px",
    fontWeight: "700",
    color: "rgb(74, 110, 224)"
  },
  indicatorItemSpan: {
    color: "#6D758D",
    fontSize: "14px",
    lineHeight: "22px",
    letterSpacing: "0px"
  },
  imageMobile: {
    maxWidth: "100%",
  }
};

// MUI style classes
const useStyles = makeStyles((theme) => ({
  overrideRoot: {
    fontFamily: `"Inter", sans-serif`
  },
  button: {
    fontFamily: "Inter,sans-serif",
    fontSize: "14px",
    lineHeight: "32px",
    "&:hover": {
      backgroundColor: "#15c39a !important"
    }
  },
  textContainer: {
    transition: "all .4s ease-in-out",
    "&:hover": {
      transform: "scale(1.05,1.05)",
      [theme.breakpoints.down("md")]: {
        transform: "none"
      }
    }
  },
  exampleImage: {
    borderRadius: "10px",
    maxWidth: "100%",
    opacity: "0",
    position: "absolute",
    top: "-50px",
    left: "20%",
    [theme.breakpoints.down("md")]: {
      maxWidth: "90%",
      left: "0"
    }
  },
  messageImage: {
    borderRadius: "10px",
    maxWidth: "100%",
    position: "absolute",
    bottom: "50px",
    left: "30%",
    opacity: "0",
    boxShadow: "0 14px 55px rgb(109 117 141 / 20%)",
    [theme.breakpoints.down("md")]: {
      maxWidth: "90%",
      bottom: "100px",
      left: "10%"
    }
  }
}));

const ImageItems = ({ index, item, classes }) => {
  return (
    <Box
      className={index === 0 ? "active" : null}
      sx={{ position: "absolute", width: "100%", height: "336px" }}
    >
      <img
        className={classes.exampleImage}
        src={item.exampleImage}
        alt={item.alt}
      />
      <img
        className={classes.messageImage}
        src={item.messageImage}
        alt={item.alt}
      />
    </Box>
  );
};

const IndicatorItems = (props) => {
  return (
    <Box sx={styling.indicatorItem} className={props.classes.textContainer}>
      <Typography sx={styling.indicatorItemHeader} variant="h5">
        {props.item.title}
      </Typography>
      <Box sx={styling.indicatorItemSpan} component="span">
        {props.item.subTitle}
      </Box>
      <Box sx={{ width: "100%", marginTop: "10px" }}>
        <LinearProgress variant="determinate" value={props.value} />
      </Box>
    </Box>
  );
};

// Feature Component
const FeaturesSection = () => {
  const classes = useStyles();
  const [progress, setProgress] = useState({
    progress0: 0,
    progress1: 0,
    progress2: 0,
    progress3: 0,
  });
  const indicatorItemContainerRef = useRef();
  const imageContainerRef = useRef();

  const changeImages = (nextProgressBarIndex) => {
    Array.from(imageContainerRef.current.childNodes).forEach((item, i) => {
      if (nextProgressBarIndex === i) {
        if (nextProgressBarIndex === 0) {
          item.classList.add("active");
          imageContainerRef.current.childNodes[3].classList.remove("active");
        } else {
          item.classList.add("active");
          imageContainerRef.current.childNodes[i - 1].classList.remove(
            "active"
          );
        }
      }
    });
  };

  useEffect(() => {
    let index = 0;
    let currentProgress = "";
    const timer = setInterval(() => {
      setProgress((oldProgress) => {
        currentProgress = `progress${index}`;
        if (oldProgress[currentProgress] === 100) {
          index < 3 ? (index += 1) : (index = 0);
          changeImages(index);
          return { progress0: progress['progress0'], progress1: progress['progress1'], progress2: progress['progress2'], 
                    progress3: progress['progress3'], [currentProgress]: 0 };
        }
        return {
          progress0: progress['progress0'], progress1: progress['progress1'], progress2: progress['progress2'], progress3: progress['progress3'],
          [currentProgress]: Math.min(oldProgress[currentProgress] + 4, 100),
        };
      });
    }, 200);

    return () => {
      clearInterval(timer);
    };
    // eslint-disable-next-line
  }, []);

  return (
    <section className={classes.overrideRoot} style={styling.section}>
      <Container
        className={classes.overrideRoot}
        maxWidth="false"
        disableGutters={true}
        sx={{ maxWidth: "1080px", position: "relative" }}
      >
        <Box sx={{ textAlign: "center" }}>
          <Typography variant="h1" sx={styling.h1}>
            Aim High With Brilliant Writing
          </Typography>
          <Button
            variant="contained"
            size="large"
            sx={styling.button}
            className={classes.button}
          >
            Upgrade to Grammarly Premium
          </Button>
        </Box>
        <Grid
          container
          justifyContent="space-between"
          sx={{
            display: "flex",
            marginTop: "100px",
            flexDirection: { xs: "column", sm: "row" },
            height: { xs: "80vh", md: "auto" }
          }}
        >
          <Grid
            item
            sm={12}
            md={6}
            sx={{ position: "relative" }}
            ref={imageContainerRef}
          >
            {previewList.map((item, i) => (
              <ImageItems
                key={i}
                index={i}
                item={item}
                classes={classes}
                imageContainerRefs={imageContainerRef}
              />
            ))}
          </Grid>
          <Grid item sm={12} md={4}>
            {featuresList.map((item, i) => (
              <IndicatorItems
                key={i}
                index={i}
                item={item}
                value={progress[`progress${i}`]}
                classes={classes}
                indicatorItemContainerRef={indicatorItemContainerRef}
              />
            ))}
          </Grid>
        </Grid>
      </Container>
    </section>
  );
};

export default FeaturesSection;
